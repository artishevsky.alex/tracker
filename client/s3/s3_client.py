import pandas as pd

from core.artifacts import BaseFabric
from core.driver import BotoS3Driver
from core.tracker import SHARED_META, TrackerNode


class S3ClientNode(TrackerNode):
    """
    S3ClientNode is a tracker client for S3.

    ## Features:
        - An ability to walk through the tree with the help of special accessor.
        - Each node has its own inner data storage (see VirtualStorage).
        - An ability to create queries on tree childs data via pandas interface.

    ## Parameters:
        - `bucket`: name of the s3 bucket.
        - `folder`: s3 folder path to the Node.
        - `name`: name of the Node.
        - `parent_node`: parent node of a current node (None if root).
        - `lazy_init`: if True, node will not be initialized on creation.

    ## Attributes:
        - storage (VirtualStorage): Getter to an inner data storage .
        - tree (TrackerNode._TreeAccessor): Getter to a special tree accessor .
        - name: name of a current node.
        - hidden:
            - `_path`: path to a folder on driver FileSystem.
            - `_provider`: FileSystem driver that implements `tracker.driver.base.BaseDriver` interface.
            - `_obj_fabric`: Object fabric that implements `tracker.artifacts.base.BaseFabric` interface.
            - `_parent`: parent node of a current node (None if root).
            - `_v_storage`: inner data storage instance.
            - `_h_tree_accessor`: special tree accessor instance.
            - `_bucket`: name of the s3 bucket.
            - `_s3_reL_path`: s3 folder path to the Node.
            - `_s3_path`: path to a folder on driver S3.

    ## Note:
        - TrackerNode is client-based only, so no synchronization is guaranteed by default.
        But you can always inherit from the original VirtualStorage,
        TrackerNode and override `push` and `fetch` methods to add synchronization
        and add callbacks on user interaction with VirtualStorage,
        TrackerNode and use `provider` that support synchronization.


    ## Example:

    ```python
    from sklearn.ensemble import RandomForestClassifier
    from tracker.artifact.serrializers import log_as_pickle

    trial = experiment.create_trial("trial-<name>")
    storage = trial.storage

    storage["description"] = "Trial for <name>"
    storage["data/x"] = [1, 2, 3]
    storage["data/y"] = [4, 5, 6]

    pipe = SomeCustomPipeline()
    model = RandomForestClassifier(n_estimators=10, random_state=42, n_jobs=-1)

    x, y = pipe.fit_transform(storage["data/x"]), storage["data/y"]
    model.fit(x, y)

    storage["model/pipeline"] = log_as_pickle(pipe)
    storage["model/predictor"] = model

    storage.push()
    ```
    """

    def __init__(self, bucket: str, folder: str, name: str, parent_node=None, lazy_init=False):
        if name.split("-")[0] != self.__meta__["node_prefix"]:
            raise ValueError(f'experiment node should satisfy "{self.__meta__["node_prefix"]}-*" format')

        self._bucket = bucket
        self._s3_reL_path = f"{folder}/{name}"
        self._s3_path = f"s3://{bucket}/{folder}/{name}"
        provider = BotoS3Driver()

        super(S3ClientNode, self).__init__(
            provider=provider,
            path=self._s3_path,
            obj_fabric=BaseFabric(),
            parent_node=parent_node,
        )

        if not lazy_init:
            try:
                self.fetch()
            except:
                pass

    def _create_child_node(self, *args, name: str = None, node_type=None, **kw):
        """
        Instantiate a child node.

        ## Parameters:
            - name (str): name of the child node.
            - node_type (str): type of the child node.
            - kw (dict): keyword arguments for the child node.
            - args (tuple): positional arguments for the child node.

        ## Returns:
            - tracker.core.tracker.entities.TrackerNode: a child node.
        """
        if name in self._get_child_names():
            raise ValueError("Child already exists")

        obj = node_type(self, *args, name=name, **kw)
        return obj

    def fetch(self):
        return super(S3ClientNode, self).fetch()

    def push(self):
        return super(S3ClientNode, self).push()


class FeatureExtractionComponent(S3ClientNode):
    """
    FeatureExtractionComponent S3 Node for feature extraction step.

    ## Features:
        - An ability to walk through the tree with the help of special accessor.
        - Each node has its own inner data storage (see VirtualStorage).
        - An ability to create queries on tree childs data via pandas interface.

    ## Parameters:
        - `trial` (Trial): parent trial.
        - `name`: name of the Node.
        - `lazy_init`: if True, node will not be initialized on creation.

    ## Attributes:
        - storage (VirtualStorage): Getter to an inner data storage .
        - tree (TrackerNode._TreeAccessor): Getter to a special tree accessor .
        - name: name of a current node.
        - hidden:
            - `_path`: path to a folder on driver FileSystem.
            - `_provider`: FileSystem driver that implements `tracker.driver.base.BaseDriver` interface.
            - `_obj_fabric`: Object fabric that implements `tracker.artifacts.base.BaseFabric` interface.
            - `_parent`: parent node of a current node (None if root).
            - `_v_storage`: inner data storage instance.
            - `_h_tree_accessor`: special tree accessor instance.
            - `_bucket`: name of the s3 bucket.
            - `_s3_reL_path`: s3 folder path to the Node.
            - `_s3_path`: path to a folder on driver S3.
    """

    __meta__ = {**SHARED_META, "node_prefix": "feature_space", "supported_children": {}}

    def __init__(self, trial, name: str, lazy_init=False):
        trial_folder = trial._s3_reL_path
        trial_bucket = trial._bucket

        super().__init__(
            bucket=trial_bucket,
            folder=trial_folder,
            name=name,
            parent_node=trial,
            lazy_init=lazy_init,
        )


class ModelingComponent(S3ClientNode):
    """
    ModelingComponent S3 Node for modeling step.

    ## Features:
        - An ability to walk through the tree with the help of special accessor.
        - Each node has its own inner data storage (see VirtualStorage).
        - An ability to create queries on tree childs data via pandas interface.

    ## Parameters:
        - `trial` (Trial): parent trial.
        - `name`: name of the Node.
        - `lazy_init`: if True, node will not be initialized on creation.

    ## Attributes:
        - storage (VirtualStorage): Getter to an inner data storage .
        - tree (TrackerNode._TreeAccessor): Getter to a special tree accessor .
        - name: name of a current node.
        - hidden:
            - `_path`: path to a folder on driver FileSystem.
            - `_provider`: FileSystem driver that implements `tracker.driver.base.BaseDriver` interface.
            - `_obj_fabric`: Object fabric that implements `tracker.artifacts.base.BaseFabric` interface.
            - `_parent`: parent node of a current node (None if root).
            - `_v_storage`: inner data storage instance.
            - `_h_tree_accessor`: special tree accessor instance.
            - `_bucket`: name of the s3 bucket.
            - `_s3_reL_path`: s3 folder path to the Node.
            - `_s3_path`: path to a folder on driver S3.
    """

    __meta__ = {**SHARED_META, "node_prefix": "ml_run", "supported_children": {}}

    def __init__(self, trial, name: str, lazy_init=False):
        trial_folder = trial._s3_reL_path
        trial_bucket = trial._bucket

        super().__init__(
            bucket=trial_bucket,
            folder=trial_folder,
            name=name,
            parent_node=trial,
            lazy_init=lazy_init,
        )

    def log_metric(self, key, value, step=None):
        """
        Logs a metric to the current modeling component.

        ## Parameters:
            - `key`: key of the metric.
            - `value`: value of the metric.
            - `step`: step of the metric.
        """
        old_value = self.storage["metrics"][key]

        if step is not None:
            row = pd.DataFrame({key: [value], "step": [step]})
            if isinstance(old_value, pd.DataFrame):
                self.storage["metrics"][key] = pd.concat([row, old_value], axis=0)
            else:
                self.storage["metrics"][key] = row
        else:
            self.storage["metrics"][key] = value


class Trial(S3ClientNode):
    """
    Trial is a particular research iteration.

    It incorporates several components:
        - FeatureSpaceComponent - for feature space.
        - ModelingComponent - for modeling.

    ## Features:
        - An ability to walk through the tree with the help of special accessor.
        - Each node has its own inner data storage (see VirtualStorage).
        - An ability to create queries on tree childs data via pandas interface.

    ## Parameters:
        - `experiment` (Trial): parent trial.
        - `name`: name of the Node.
        - `lazy_init`: if True, node will not be initialized on creation.

    ## Attributes:
        - storage (VirtualStorage): Getter to an inner data storage .
        - tree (TrackerNode._TreeAccessor): Getter to a special tree accessor .
        - name: name of a current node.
        - hidden:
            - `_path`: path to a folder on driver FileSystem.
            - `_provider`: FileSystem driver that implements `tracker.driver.base.BaseDriver` interface.
            - `_obj_fabric`: Object fabric that implements `tracker.artifacts.base.BaseFabric` interface.
            - `_parent`: parent node of a current node (None if root).
            - `_v_storage`: inner data storage instance.
            - `_h_tree_accessor`: special tree accessor instance.
            - `_bucket`: name of the s3 bucket.
            - `_s3_reL_path`: s3 folder path to the Node.
            - `_s3_path`: path to a folder on driver S3.
    """

    __meta__ = {
        **SHARED_META,
        "node_prefix": "trial",
        "supported_children": {
            "ml_run": ModelingComponent,
            "feature_space": FeatureExtractionComponent,
        },
    }

    def __init__(self, experiment, name: str, lazy_init: bool = False):
        experiment_folder = experiment._s3_reL_path
        experiment_bucket = experiment._bucket

        super().__init__(
            bucket=experiment_bucket,
            folder=experiment_folder,
            name=name,
            parent_node=experiment,
            lazy_init=lazy_init,
        )

    def create_feature_component(self, component_name: str, **component_kwargs):
        """
        Creates a new FeatureSpaceComponent.

        ## Parameters:
            - `component_name`: name of the component.
            - `component_kwargs`: keyword arguments for the component.

        ## Returns:
            - `obj`: created component.
        """
        obj = self._create_child_node(
            name=component_name,
            node_type=FeatureExtractionComponent,
            lazy_init=False,
            **component_kwargs,
        )

        return obj

    def create_modeling_component(self, component_name: str, **component_kwargs):
        """
        Creates a new ModelingComponent.

        ## Parameters:
            - `component_name`: name of the component.
            - `component_kwargs`: keyword arguments for the component.

        ## Returns:
            - `obj`: created component.
        """
        obj = self._create_child_node(
            name=component_name,
            node_type=ModelingComponent,
            lazy_init=False,
            **component_kwargs,
        )

        return obj

    def get_component(self, component_name: str):
        return self._get_child(component_name, lazy_init=False)


class Experiment(S3ClientNode):
    """
    Experiment is a project level node.

    It incorporates trial which is a particular research iteration.

    ## Features:
        - An ability to walk through the tree with the help of special accessor.
        - Each node has its own inner data storage (see VirtualStorage).
        - An ability to create queries on tree childs data via pandas interface.

    ## Parameters:
        - `bucket`: name of the s3 bucket.
        - `folder`: s3 folder path to the Node.
        - `name`: name of the Node.


    ## Attributes:
        - storage (VirtualStorage): Getter to an inner data storage .
        - tree (TrackerNode._TreeAccessor): Getter to a special tree accessor .
        - name: name of a current node.
        - hidden:
            - `_path`: path to a folder on driver FileSystem.
            - `_provider`: FileSystem driver that implements `tracker.driver.base.BaseDriver` interface.
            - `_obj_fabric`: Object fabric that implements `tracker.artifacts.base.BaseFabric` interface.
            - `_parent`: parent node of a current node (None if root).
            - `_v_storage`: inner data storage instance.
            - `_h_tree_accessor`: special tree accessor instance.
            - `_bucket`: name of the s3 bucket.
            - `_s3_reL_path`: s3 folder path to the Node.
            - `_s3_path`: path to a folder on driver S3.

    """

    __meta__ = {
        **SHARED_META,
        "node_prefix": "experiment",
        "supported_children": {"trial": Trial},
    }

    def __init__(self, bucket: str, folder: str, name: str):
        super().__init__(bucket=bucket, folder=folder, name=name)

    def create_trial(self, trial_name: str, **trial_kwargs):
        obj = self._create_child_node(name=trial_name, node_type=Trial, lazy_init=False, **trial_kwargs)

        return obj

    def get_trial(self, trial_name: str):
        return self._get_child(trial_name, lazy_init=False)
