class BaseSerializer:
    """
    BaseSerializer is a base class for all serializers.

    ## Attributes:
    __meta__ (dict): a dictionary of meta information about a serializer:
     - `supported_objs` (list): a list of supported objects
     - `fp_io_mode` ("binary" | "text"):  - mode for file operations
    """

    __meta__ = {"supported_objs": [], "fp_io_mode": "binary"}

    def _dump(self, obj, fp):
        """
        Overriden method of the load of the artifact to the provider.

        ## Parameters:
            - obj (object): an object to be dumped.
            - fp (file): a file object for the artifact.

        ## Returns:
            - fp (file): a file object for the artifact.
        """
        raise NotImplementedError()

    def _load(self, fp):
        """
        Overriden method of the load of the artifact to the provider.

        ## Parameters:
            - fp (file): a file object for the artifact.

        ## Returns:
            - obj (object): an object loaded from the artifact.
        """
        raise NotImplementedError()

    def dump(self, obj, fp):
        """
        Dump the artifact to the provider.

        ## Parameters:
            - obj (object): an object to be dumped.
            - fp (file): a file object for the artifact.

        ## Returns:
            - fp (file): a file object for the artifact.
        """
        fp.seek(0)
        self._dump(obj, fp)
        fp.seek(0)
        return fp

    def load(self, fp):
        """
        Load the artifact from the provider.

        ## Parameters:
            - fp (file): a file object for the artifact.

        ## Returns:
            - obj (object): an object loaded from the artifact.
        """
        fp.seek(0)
        obj = self._load(fp)
        fp.seek(0)
        return obj
