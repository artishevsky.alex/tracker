import joblib

from lightgbm.sklearn import LGBMClassifier, LGBMRanker, LGBMRegressor
from sklearn.linear_model import (
    ElasticNet,
    Lasso,
    LinearRegression,
    LogisticRegression,
    Ridge,
)
from sklearn.pipeline import Pipeline

from core.artifacts.serializers import BaseSerializer

__doc__ = """
    Serializers:

    - BaseEstimatorSerializer:
        - sklearn.linear_model.LinearRegression
        - sklearn.linear_model.Ridge
        - sklearn.linear_model.Lasso
        - sklearn.linear_model.ElasticNet
        - sklearn.linear_model.LogisticRegression
        - lightgbm.sklearn.LGBMRegressor
        - lightgbm.sklearn.LGBMClassifier
        - lightgbm.sklearn.LGBMRanker
        - sklearn.pipeline.Pipeline
"""


class BaseEstimatorSerializer(BaseSerializer):
    __meta__ = {
        "supported_objs": [
            LGBMRegressor,
            LGBMClassifier,
            LGBMRanker,
            LinearRegression,
            Ridge,
            Lasso,
            ElasticNet,
            LogisticRegression,
            Pipeline,
        ],
        "fp_io_mode": "binary",
    }

    def _dump(self, obj, fp):
        joblib.dump(obj, fp)

    def _load(self, fp):
        return joblib.load(fp)
