from core.artifacts.serializers.base import BaseSerializer
from core.artifacts.serializers.meta import (
    PandasDataFrameSerializer,
    PickleSerializer,
    PILImageSerializer,
    log_as_pickle,
)
from core.artifacts.serializers.transformers import BaseEstimatorSerializer


class SerializerRepo:
    """
    SerializerRepo is a repository of tracker serializers.

    ## Attributes:
        - repository (dict): a dictionary of serializers
        - hidden:
            - _dtype_to_serializer__schema (dict): a mapping of dtype to serializer name
            - _serializer_to_dtype__schema (list): a list of supported dtypes for a particular serializer
    """

    def __init__(self):
        self.repository = {}

        self._dtype_to_serializer__schema = {}
        self._serializer_to_dtype__schema = {}

    def __getitem__(self, key):
        """
        Returns a serializer instance with a given name.

        ## Parameters:
            - key (str): a name of a serializer

        ## Returns:
            - tracker.core.artifacts.serializers.base.BaseSerializer: a serializer instance
        """
        return self.repository[key]

    def register(self, name: str):
        """
        Prepare registry function for a serializer with a given name.

        ## Parameters:
            - name (str): a name of a serializer

        ## Second call parameters:
            - serializer (tracker.core.artifacts.serializers.base.BaseSerializer): a serializer instance

        ## Returns:
            - register_func (function): a function for registration of a serializer

        ## Example:
        ```python

        repo.register('PandasDataFrameSerializer')(PandasDataFrameSerializer)

        ```
        """

        def decorator(obj):
            self.repository[name] = obj
            self._register_serializer(obj, name)
            return obj

        return decorator

    def _register_serializer(self, serializer, name: str):
        """
        Register a serializer with a given name.

        ## Parameters:
            - serializer (tracker.core.artifacts.serializers.base.BaseSerializer): a serializer instance
            - name (str): a name of a serializer
        """
        supported_types = list(serializer.__meta__["supported_objs"])

        for dtype in supported_types:
            self._dtype_to_serializer__schema[repr(dtype)] = name

        self._serializer_to_dtype__schema = supported_types


serializer_repo = SerializerRepo()

serializer_repo.register("PandasDataFrameSerializer")(PandasDataFrameSerializer())
serializer_repo.register("PILImageSerializer")(PILImageSerializer())
serializer_repo.register("BaseEstimatorSerializer")(BaseEstimatorSerializer())
serializer_repo.register("PickleSerializer")(PickleSerializer())
