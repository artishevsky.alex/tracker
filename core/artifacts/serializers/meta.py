import pickle

import pandas as pd

from PIL import Image

from core.artifacts.serializers import BaseSerializer

__doc__ = """
    Serializers:

    - PandasDataFrameSerializer -> pandas.DataFrame
    - PILImageSerializer -> PIL.Image
    - PickleSerializer -> log_as_pickle - serialize any object as pickle
"""


class PandasDataFrameSerializer(BaseSerializer):
    __meta__ = {
        "supported_objs": [
            pd.DataFrame,
        ],
        "fp_io_mode": "text",
    }

    def _dump(self, obj, fp):
        obj.to_csv(fp, index=False)

    def _load(self, fp):
        return pd.read_csv(fp)


class PILImageSerializer(BaseSerializer):
    __meta__ = {
        "supported_objs": [
            Image.Image,
        ],
        "fp_io_mode": "text",
    }

    def _dump(self, obj, fp):
        obj.save(fp)

    def _load(self, fp):
        # In terms of issues see PIL.Image.open docs
        return Image.open(fp)


class PickleSerializer(BaseSerializer):
    """
    PickleSerializer is a universal serializer for any object.

    Note that it is not a good idea to use it for large objects,
    and it isn't configured, so it has many limitations.
    """

    __meta__ = {"supported_objs": [], "fp_io_mode": "binary"}

    def _dump(self, obj, fp):
        pickle.dump(obj, fp)

    def _load(self, fp):
        return pickle.load(fp)


def log_as_pickle(obj):
    return {"__serializer__": "PickleSerializer", "parsed_obj": obj}
