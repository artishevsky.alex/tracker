import tempfile

from core.artifacts.serializers import serializer_repo


class BaseFabric:
    """
    BaseFabric is a default fabric for all allowed artifacts.

    It is responsible for object serialization and deserialization
    through a recipe and a provider (see `tracker.core.driver.base.BaseDriver`).

    ## Attributes:
        - repo (tracker.core.artifacts.serializers.SerializerRepo): a repository of serializers
    """

    repo = serializer_repo

    def supports(self, obj):
        """Returns True if the object is supported by serializer instances in repo."""
        return repr(type(obj)) in self.repo._dtype_to_serializer__schema.keys()

    def _serializer_from_obj(self, obj):
        """Returns a serializer instance for the given object."""
        obj_dtype = repr(type(obj))
        return self.repo._dtype_to_serializer__schema.get(obj_dtype, None)

    def dump(self, recipe: dict, provider):
        """
        Dump the artifact to the provider.

        ## Parameters:
            - recipe (dict): a recipe for the artifact.
             Structure: {'artifact_path': <str>, 'v_path': <str>, 'parsed_obj': <obj>}
            - provider (tracker.core.driver.base.BaseDriver): a provider for the artifact.
        """
        serializer = self.repo[recipe["__serializer__"]]

        io_mode = serializer.__meta__["fp_io_mode"]
        _bin_postfix = "" if io_mode == "text" else "b"

        with tempfile.TemporaryFile("w+" + _bin_postfix) as fp:
            serializer.dump(obj=recipe["parsed_obj"], fp=fp)

            provider.write(path=f'{recipe["artifact_path"]}/{recipe["v_path"]}', fp=fp)

    def load(self, recipe: dict, provider):
        """
        Load the artifact from the provider.

        ## Parameters:
            - recipe (dict): a recipe for the artifact.
             Structure: {'artifact_path': <str>, 'v_path': <str>, 'parsed_obj': <obj>}
            - provider (tracker.core.driver.base.BaseDriver): a provider for the artifact.
        """
        serializer = self.repo[recipe["__serializer__"]]

        io_mode = serializer.__meta__["fp_io_mode"]
        _bin_postfix = "" if io_mode == "text" else "b"

        with tempfile.TemporaryFile("r+" + _bin_postfix) as fp:
            provider.read(path=f'{recipe["artifact_path"]}/{recipe["v_path"]}', fp=fp)

            obj = serializer.load(fp)
            recipe["parsed_obj"] = obj

        return recipe
