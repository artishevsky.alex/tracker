import json
import re
import tempfile

from typing import Iterable, Union
from warnings import WarningMessage

import numpy as np
import pandas as pd

SHARED_META = {
    "v_storage_folder": "__storage__",
    "v_storage_filename": "v_storage.json",
}


class VirtualStorage:
    """
    `TrackerNode's` virtual storage

    Virtual storage is a metadata storage that implemented is in a `FileSystem` manner and
    can be used to store artifacts, metrics, dataframes, models
    and other useful metadata about you ML steps.

    ## Features:
     - It's tree structure is recurrent and one can see some
        similarities with a python `default dict` The aim is to make
    creation process of new folders (child `VirtualStorage` nodes) automatic.
     - VirtualStorage can be serialized and deserialized to/from json via special `provider`
      (see `tracker.driver.base.BaseDriver`) to a driver FS.
     - It might also work with non-json serializable objects if they are supported by `obj_fabric`
      (see `tracker.artifacts.base.BaseFabric`).
     It just saves the object to a driver FS and then loads it from there by parsing special recipe.


    ## Parameters:
        - `fs_path`: Driver FileSystem path to the location where VirtualStorage json representation is stored.
        - `provider`: FileSystem driver that implements `tracker.driver.base.BaseDriver` interface.
        - `meta`: Dictionary with metadata about VirtualStorage.
         It defines v_storage_folder and v_storage_filename that driver should search serialized VirtualStorage on FS.
        - `obj_fabric`: Object fabric that implements `tracker.artifacts.base.BaseFabric` interface.
        - `v_path`: path from the root VirtualStorage node.

    ## Attributes:
        - `_storage`: Dictionary with VirtualStorage nodes or data leafs.
        - `_v_path`: path from the root VirtualStorage node.
        - `_fs_path`: Driver FileSystem path to the location where VirtualStorage json representation is stored.
        - `_provider`: FileSystem driver that implements `tracker.driver.base.BaseDriver` interface.
        - `_meta`: Dictionary with metadata about VirtualStorage.

    ## Note:

    VirtualStorage along with TrackerNode is client-based only, so no synchronization is guaranteed by default.
    But you can always inherit from the original VirtualStorage
    and override `push` and `fetch` methods to add synchronization
    and add callbacks on user interaction with VirtualStorage and use `provider` that support synchronization.

    ## Example:

    ```python
    from sklearn.ensemble import RandomForestClassifier
    from tracker.artifact.serializers import log_as_pickle

    trial = experiment.get_trial("trial-<name>")
    storage = trial.storage

    storage["description"] = "Trial for <name>"
    storage["data/x"] = [1, 2, 3]
    storage["data/y"] = [4, 5, 6]

    pipe = SomeCustomPipeline()
    model = RandomForestClassifier(n_estimators=10, random_state=42, n_jobs=-1)

    x, y = pipe.fit_transform(storage["data/x"]), storage["data/y"]
    model.fit(x, y)

    storage["model/pipeline"] = log_as_pickle(pipe)
    storage["model/predictor"] = model

    storage.push()
    ```
    """

    def __init__(self, fs_path, provider, meta, obj_fabric, v_path=""):
        """Initialize VirtualStorage node"""

        self._provider = provider
        self._fabric = obj_fabric

        self._v_path = v_path
        self._fs_path = fs_path

        self._meta = meta
        self._storage = {}

    def fetch(self, raw_prefetch=False):
        """
        Fetch VirtualStorage data from the driver FS.

        ## Parameters:
            - `raw_prefetch`: If True, then VirtualStorage data is fetched
            from the driver FS without parsing recipes into original objects.

        ## Raises:
            - `AssertionError`: If fetch was called on non-root VirtualStorage node.


        """
        if self._v_path == "":
            meta = self._meta

            with tempfile.TemporaryFile(mode="r+") as fp:
                self._provider.read(
                    path=f'{self._fs_path}/{meta["v_storage_folder"]}/{meta["v_storage_filename"]}',
                    fp=fp,
                )

                self._storage = self.__dict_to_node(node=json.load(fp), v_path=self._v_path, raw_prefetch=raw_prefetch)
        else:
            raise AssertionError("Storage should be root")

    def push(self):
        """
        Push VirtualStorage data to the driver FS.

        ## Raises:
            - `AssertionError`: If push was called on non-root VirtualStorage node.
        """
        if self._v_path == "":
            meta = self._meta

            with tempfile.TemporaryFile(mode="w+") as fp:
                json.dump(
                    obj=self.__node_to_dict(node=self, v_path=self._v_path, raw_prefetch=False),
                    fp=fp,
                )

                self._provider.write(
                    path=f'{self._fs_path}/{meta["v_storage_folder"]}/{meta["v_storage_filename"]}',
                    fp=fp,
                )
        else:
            raise AssertionError("Storage should be root")

    def keys(self):
        """Return keys of the VirtualStorage"""
        return self._storage.keys()

    def values(self):
        """Return values of the VirtualStorage"""
        return self._storage.values()

    def items(self):
        """Return items of the VirtualStorage"""
        return self._storage.items()

    def create_obj(self, recipe):
        """Create object by a given recipe

        ## Parameters:
            - `recipe`: Recipe that `obj_fabric` how to create an object.

        ## Returns:
            - `obj`: Object that was created by `obj_fabric`.
        """
        self._fabric.load(recipe, self._provider)

    def to_dict(self, raw_prefetch: bool = True):
        """
        Convert VirtualStorage to a flat dictionary with keys
        that are paths to VirtualStorage leafs and values that are leafs themselves.

        ## Parameters:
         - `raw_prefetch`: If True, then recipes wouldn't be parsed into original objects.

        ## Returns:
            - `dict`: Dictionary with VirtualStorage data.
        """
        return self.__node_to_dict(self, raw_prefetch=raw_prefetch, v_path="")

    def _is_primitive(self, el):
        """Check if an element is an allowed primitive"""
        if isinstance(el, str) or isinstance(el, int) or isinstance(el, float) or isinstance(el, bool) or el is None:
            return True
        return False

    def __node_to_dict(self, node, v_path: str, raw_prefetch: bool = True):
        """
        Recurrently convert VirtualStorage node to a flat dictionary

        ## Parameters:
            - `node`: VirtualStorage node.
            - `v_path`: path from the root VirtualStorage node.
            - `raw_prefetch`: If True, then recipes wouldn't be parsed into original objects.

        ## Returns:
            - `dict`: Dictionary with VirtualStorage data.

        ## Note:
        DO NOT USE THIS METHOD. NEVER. USE `to_dict` instead.
        """

        # We met v_storage node
        if isinstance(node, VirtualStorage) or (v_path == "" and isinstance(node, dict)):
            dict_node = {
                _k: self.__node_to_dict(node=_v, v_path=f"{v_path}/{_k}")
                for _k, _v in node._storage.items()
                if isinstance(_k, str)
            }
            return dict_node
        # We met recipe
        elif isinstance(node, dict) and node.get("__serializer__", None) is not None:
            if node.get("parsed_obj", None) is None:
                raise RuntimeError(f"Met an empty recipe {node}")

            # Paths are calculated at the time call
            node["v_path"] = v_path.strip("/").rstrip("/")
            node["artifact_path"] = f'{self._fs_path}/{self._meta["v_storage_folder"]}/artifacts/'.strip("/").rstrip(
                "/"
            )

            self._fabric.dump(node, self._provider)

            clean_node = node.copy()
            del clean_node["parsed_obj"]

            return clean_node
        else:
            return node

    def __dict_to_node(self, node, v_path="", raw_prefetch=True):
        """
        Recurrently convert dictionary to VirtualStorage node.

        ## Parameters:
            - `node`: Dictionary with VirtualStorage data.
            - `v_path`: path from the root VirtualStorage node.
            - `raw_prefetch`: If True, then recipes wouldn't be parsed into original objects.

        ## Returns:
            - `VirtualStorage`: VirtualStorage node.

        ## Note:
        DO NOT USE THIS METHOD. NEVER. THERE IS NO NEED TO USE IT. I SWEAR.
        """

        if isinstance(node, list):
            return [self.__parse_item_on_set(obj=el, json_primitive_only=True) for el in node]
        if isinstance(node, dict):
            if node.get("__serializer__", None) is not None:
                if raw_prefetch:
                    return node
                else:
                    return self._fabric.load(node, self._provider)
            else:
                if v_path != "":
                    virtual_storage_node = VirtualStorage(
                        v_path=v_path,
                        fs_path=self._fs_path,
                        provider=self._provider,
                        meta=self._meta,
                        obj_fabric=self._fabric,
                    )
                else:
                    virtual_storage_node = {}

                for _k, _v in node.items():
                    if isinstance(_k, str):
                        virtual_storage_node[_k] = self.__dict_to_node(
                            node=_v,
                            v_path=f"{self._v_path}/{_k}" if self._v_path != "" else _k,
                        )
                return virtual_storage_node
        else:
            return node

    def __parse_item_on_get(self, obj):
        """
        Parse item on get in order to parse probable recipes

        ## Parameters:
            - `obj`: Object to parse.

        ## Returns:
            - `obj`: Parsed object.
        """
        if isinstance(obj, dict) and obj.get("__serializer__", None) is not None:
            if not obj.get("parsed_obj", None) is None:
                return obj["parsed_obj"]
            else:
                self._fabric.load(obj, self._provider)
                return obj["parsed_obj"]
        else:
            return obj

    def __parse_item_on_set(self, obj, json_primitive_only: bool = False):
        """
        Parse item on set in order to parse probable serializable objects

        ## Parameters:
            - `obj`: Object to parse.
            - `json_primitive_only`: If True, then only JSON primitives will be parsed.

        ## Returns:
            - `obj`: Parsed object.
        """
        if self._is_primitive(obj):
            return obj
        elif isinstance(obj, VirtualStorage) and not json_primitive_only:
            return obj
        elif not json_primitive_only and self._fabric.supports(obj):
            parsing_candidate = self._fabric._serializer_from_obj(obj)

            # Serializer exists
            if parsing_candidate is not None:
                recipe = {"__serializer__": parsing_candidate, "parsed_obj": obj}
                return recipe
            raise ValueError(f"Unsupported type of v_storage item: {type(obj)}")
        elif isinstance(obj, Iterable):
            if isinstance(obj, dict):
                if obj.get("__serializer__", None) is not None:
                    # We treat obj as recipe and set it as is
                    return obj
                else:
                    return {
                        _k: self.__parse_item_on_set(_v, json_primitive_only=True)
                        for _k, _v in obj.items()
                        if isinstance(_k, str)
                    }
            else:
                return [self.__parse_item_on_set(_el, json_primitive_only=True) for _el in obj]

        if json_primitive_only:
            raise KeyError("U can set serializable objects only within a v_storage value")
        raise ValueError(f"Unsupported type of v_storage item: {type(obj)}")

    def __getitem__(self, __k):
        """
        Get item from VirtualStorage node.

        ## Parameters:
            - `__k`: v_path to the item.

        ## Returns:
            - `obj`: Object from VirtualStorage node.
        """
        if re.match(r"^(?:[a-zA-Z0-9_])+", __k) is None:
            raise KeyError("Invalid key, accessible symbols: a-z, A-Z, 0-9 and _")

        v_path_keys = __k.split("/")

        _key = v_path_keys[0]
        _val = self._storage.get(_key, None)

        # We got item here
        if _val is None and not isinstance(_val, VirtualStorage):
            if len(v_path_keys) != 1:
                raise KeyError(f"Attempted to access {v_path_keys} while {_key} is already used by non-storage object")
            return self.__parse_item_on_get(_val)

        if _val is None:
            _val = self._storage[_key] = VirtualStorage(
                v_path=f"{self._v_path}/{_key}",
                fs_path=self._fs_path,
                provider=self._provider,
                meta=self._meta,
                obj_fabric=self._fabric,
            )

        if len(v_path_keys) == 1:
            return self.__parse_item_on_get(_val)
        else:
            return _val["/".join(v_path_keys[1:])]

    def __setitem__(self, __k, __v):
        """
        Set item to VirtualStorage node.

        ## Parameters:
            - `__k`: v_path to the item.
            - `__v`: Object to set.
        """
        if re.match(r"^(?:[a-zA-Z0-9_])+", __k) is None:
            raise KeyError("Invalid key, accessible symbols: a-z, A-Z, 0-9 and _")

        v_path_keys = __k.split("/")

        _key = v_path_keys[0]
        _val = self._storage.get(_key, None)

        if len(v_path_keys) == 1:
            self._storage[_key] = self.__parse_item_on_set(__v)
            return

        if _val is None:
            self._storage[_key] = VirtualStorage(
                v_path=f"{self._v_path}/{_key}",
                fs_path=self._fs_path,
                provider=self._provider,
                meta=self._meta,
                obj_fabric=self._fabric,
            )

        self._storage[_key]["/".join(v_path_keys[1:])] = self.__parse_item_on_set(__v)

    def __str__(self):
        return str(self._storage.keys())

    def __repr__(self) -> str:
        return self.__str__()

    def delete(self, path: str):
        """
        Delete item from VirtualStorage node by a given path.

        ## Parameters:
            - `path`: v_path to the item.
        """
        v_path_keys = path.split("/")

        next_key = v_path_keys[0]

        if len(v_path_keys) == 1:
            del self._storage[next_key]
        else:
            self._storage[next_key].delete("/".join(v_path_keys[1:]))

    def to_flatten_dict(self):
        """
        Convert VirtualStorage node to a flat dictionary.

        ## Returns:
            - `dict`: Flat dictionary.
        """
        flatten = {}

        for _k, _v in self._storage.items():
            if isinstance(_v, VirtualStorage):
                sub_flatten = _v.to_flatten_dict()
                for _suffix, _value in sub_flatten.items():
                    flatten[f"{_k}/{_suffix}"] = _value
            else:
                flatten[f"{_k}"] = _v

        return flatten


class TrackerNode:
    """
    `TrackerNode` is a node of a tracker hierarchical tree.

    `TrackerNode` is a base for a semantic part of an experiment. Hierarchical tree structure by itself
    is defined by driver FileSystem inner structure. Each child is a folder in the filesystem
    with a specific prefix at the beginning (see TrackerNode.__meta__["supported_children"]).

    ## Features:
        - An ability to walk through the tree with the help of special accessor.
        - Each node has its own inner data storage (see VirtualStorage).
        - An ability to create queries on tree childs data via pandas interface.

    ## Parameters:
        - `path`: path to a folder on driver FileSystem.
        - `provider`: FileSystem driver that implements `tracker.driver.base.BaseDriver` interface.
        - `obj_fabric`: Object fabric that implements `tracker.artifacts.base.BaseFabric` interface.
        - `parent_node`: parent node of a current node (None if root).

    ## Attributes:
        - storage (VirtualStorage): Getter to an inner data storage .
        - tree (TrackerNode._TreeAccessor): Getter to a special tree accessor .
        - name: name of a current node.
        - hidden:
            - `_path`: path to a folder on driver FileSystem.
            - `_provider`: FileSystem driver that implements `tracker.driver.base.BaseDriver` interface.
            - `_obj_fabric`: Object fabric that implements `tracker.artifacts.base.BaseFabric` interface.
            - `_parent`: parent node of a current node (None if root).
            - `_v_storage`: inner data storage instance.
            - `_h_tree_accessor`: special tree accessor instance.

    ## Note:
        - If node is not a root node, then it might be created only from a parent node.
        - Be careful with the naming of nodes to avoid conflicts
         (since there is no native synchronization) and align with the allowed prefixes.
        - TrackerNode is client-based only, so no synchronization is guaranteed by default.
        But you can always inherit from the original VirtualStorage,
        TrackerNode and override `push` and `fetch` methods to add synchronization
        and add callbacks on user interaction with VirtualStorage,
        TrackerNode and use `provider` that support synchronization.


    ## Example:

    ```python
    from sklearn.ensemble import RandomForestClassifier
    from tracker.artifact.serializers import log_as_pickle

    trial = experiment.create_trial("trial-<name>")
    storage = trial.storage

    storage["description"] = "Trial for <name>"
    storage["data/x"] = [1, 2, 3]
    storage["data/y"] = [4, 5, 6]

    pipe = SomeCustomPipeline()
    model = RandomForestClassifier(n_estimators=10, random_state=42, n_jobs=-1)

    x, y = pipe.fit_transform(storage["data/x"]), storage["data/y"]
    model.fit(x, y)

    storage["model/pipeline"] = log_as_pickle(pipe)
    storage["model/predictor"] = model

    storage.push()
    ```


    """

    __meta__ = {**SHARED_META, "supported_children": {}}

    class _TreeAccessor:
        """
        Special accessor for a tracker hierarchical tree.

        It is used to walk through the tree and access child nodes along with creating queries on child nodes data.
        Queries are just pandas snapshots of children virtual storages that can be accessed via pandas.

        ## Parameters:
            - `root_node`: node that would treat as a root at the moment of an access.

        ## Attributes:
            - `root_node`: root at the moment of access.

        ## Example:
        ```python

        trial_view = experiment.tree.child_view(indent_lvl=1, prefixes=['trial'])

        rfr_trials = trial_view[trial_view["model/type"] == "RandomForestClassifier"]
        for trial_data in rfr_trials:
            print(experiment.tree.get_child(trial_data.index).name)
        ```
        """

        class _SnapshotView:
            """
            Child data snapshot view.

            ## Parameters:
                - `data`: raw children data in the following format:

            ```python
            data_sample = [
                ('trial-1-rfr', ['trial'], {'model/type': 'RandomForestClassifier'}),
                ('trial-2-xgb', ['trial'], {'model/type': 'XGBClassifier'}),
            ]
            ```

            ## Attributes:
                - `pd_view`: pandas view of the children virtual storage.
            """

            def __init__(self, data):
                self.__meta_as_pd_view = lambda flatten_dict: pd.DataFrame(
                    data=np.array([list(flatten_dict.values())]),
                    columns=flatten_dict.keys(),
                )

                if len(data) <= 0:
                    self.pd_view = pd.DataFrame()
                    return

                idx_cols = (
                    [f"{idx_col}_id" for idx_col in data[0][1]] if not isinstance(data[0][1], str) else [data[0][1]]
                )

                indexed_rows = []
                for raw_data_row in data:
                    row_idx = raw_data_row[0] if len(idx_cols) > 1 else [raw_data_row[0]]
                    row_data = raw_data_row[2]

                    for idx_col, idx_val in zip(idx_cols, row_idx):
                        row_data[idx_col] = idx_val
                    indexed_rows.append(self.__meta_as_pd_view(row_data))

                self.pd_view = pd.concat(indexed_rows, axis=0).set_index(idx_cols)

            def search(self, predicate):
                """
                Apply a predicate on the children data snapshot.

                ## Parameters:
                    - `predicate`: a function that takes a pandas row and returns a boolean.

                ## Returns:
                    - `pd_view`: pandas view of the children virtual storage that satisfy the predicate.
                """
                try:
                    mask = predicate(self.pd_view)
                except Exception:
                    raise WarningMessage("Wrong pandas predicate occured, raising an exception")

                return self.pd_view[mask].index.reset_index()

            def view(self):
                """Returns a pandas view of the children data snapshot."""
                return self.pd_view

        def __init__(self, root_node):
            self._root = root_node

        def __fetch_hierarchy_components(self, indent_lvl: int = 1, prefixes: Union[list, None] = None):
            """
            Load all children of a node and return them as a list of tuples: (node_name, node_prefixes, node_obj).

            ## Parameters:
                - `indent_lvl`: relative indent level from the root node.
                - `prefixes`: list of allowed prefixes.

            ## Returns:
                - `components`: list of tuples: (node_name, node_prefixes, node_obj).
            """
            component_names = self._root._get_child_names()

            components = []
            if indent_lvl > 0:
                for component_id in component_names:
                    component_obj = self._root._get_child(
                        component_id,
                        prefetched_children=component_names,
                        lazy_init=True,
                    )

                    component_prefix = component_id.split("-")[0]

                    if indent_lvl > 1:
                        component_response = component_obj._h_tree_accessor.__fetch_hierarchy_components(
                            indent_lvl - 1, prefixes=prefixes
                        )

                        # gson stands for "grandson"
                        for gson_id, gson_prefix, gson_obj in component_response:
                            gson_id = (component_id, gson_id)
                            gson_prefix = (component_prefix, gson_prefix)

                            if prefixes is None or gson_prefix[-1] in prefixes:
                                components.append((gson_id, gson_prefix, gson_obj))

                    else:
                        if prefixes is not None or component_prefix in prefixes:
                            components.append((component_id, component_prefix, component_obj))

                return components

            raise ValueError(f"Param indent_lvl should greater than zero but got {indent_lvl}")

        def child_view(self, prefixes, indent_lvl: int = 1, n_jobs: int = 10):
            """
            Returns a child view of the experiment tree.

            ## Parameters:
                - `prefixes`: list of allowed prefixes.
                - `indent_lvl`: relative indent level from the root node.
                - `n_jobs`: number of parallel jobs to run [NOT IMPLEMENTED YET].

            ## Returns:
                - `child_view` (`TrackerNode._TreeAccessor._SnapshotView`): child view of the experiment tree.
            """

            components = self.__fetch_hierarchy_components(indent_lvl=indent_lvl, prefixes=prefixes)

            for component in components:
                component[2].fetch()

            return self._SnapshotView(
                [(component[0], component[1], component[2].storage.to_flatten_dict()) for component in components]
            )

        def parent(self, reload: bool = False):
            """Returns the parent node of the current node."""
            if self._root._parent is None:
                raise IndexError("Already root")

            if reload:
                self._root._parent.fetch()

            return self._root._parent

        def get_child(self, index, indent_lvl: int = 1):
            """
            Returns a child node of the current node with a given name and an indent level.

            ## Parameters:
                - `index`: child node path. Example: ['<lvl_1 name>', '<lvl_2 name>'].
                - `indent_lvl`: relative indent level from the root node.

            ## Returns:
                - `child` (`TrackerNode`): child node.
            """
            if indent_lvl != 1:
                components = self.__fetch_hierarchy_components(indent_lvl=indent_lvl)

                idx_len = None
                if len(components) > 0:
                    idx_len = len(components[0][0])

                for component_idxs, _, component_obj in components:
                    counter = 0
                    for idx_l, idx_r in zip(component_idxs, index):
                        if idx_l == idx_r:
                            counter += 1
                    if counter == idx_len:
                        return component_obj

                raise KeyError(f"Unknown or unsupported child: {index}")
            return self._root._get_child(index)

    def __init__(self, provider, path, obj_fabric, parent_node=None):
        self._path = path
        self._provider = provider

        self._parent = parent_node
        self._fabric = obj_fabric

        self._v_storage = VirtualStorage(
            fs_path=path,
            v_path="",
            provider=provider,
            meta=self.__meta__,
            obj_fabric=obj_fabric,
        )

        # User accessible storage
        self._v_storage["u_space"] = VirtualStorage(
            fs_path=path,
            v_path="u_space",
            provider=provider,
            meta=self.__meta__,
            obj_fabric=obj_fabric,
        )
        self._v_storage["artifacts"] = []

        self._h_tree_accessor = self._TreeAccessor(self)

        self.name = path.split("/")[-1]

    def fetch(self):
        """Fetches the node data."""
        self._v_storage.fetch()

    def push(self):
        """Pushes the node data."""
        self._v_storage.push()

    def _get_child_names(self):
        """Returns a list of child node names."""
        supported_prefixes = [prefix for prefix in self.__meta__["supported_children"].keys()]

        return list(
            set(
                child.rstrip("/")
                for child in filter(
                    lambda file: (file[-1] == "/") and (file.split("-")[0] in supported_prefixes),
                    self._provider.list(self._path),
                )
            )
        )

    def _get_child(
        self,
        key: str,
        prefetched_children: Union[list, None] = None,
        lazy_init: bool = False,
    ):
        """
        Returns a child node with a given name.

        ## Parameters:
            - `key`: child node name.
            - `prefetched_children`: list of child names.
            - `lazy_init`: if True, the child node will not be instantly initialized.

        ## Returns:
            - `child` (`TrackerNode`): child node.
        """
        if prefetched_children is None:
            prefetched_children = self._get_child_names()

        prefix = key.split("-")[0]

        if key in prefetched_children:
            return self.__meta__["supported_children"][prefix](self, name=key, lazy_init=lazy_init)

        raise KeyError(f"Unknown or unsupported child: {key}")

    @property
    def __get_v_storage(self):
        """Returns the user accessible storage."""
        return self._v_storage["u_space"]

    @property
    def __get_h_tree(self):
        """Returns the hierarchical tree."""
        return self._h_tree_accessor

    def log_artifact(self, path: str, fp):
        """
        Attach an artifact to the node.

        Artifact will be stored on driver FS with a path:
        '<path to the node>/<v_storage_folder>/artifacts/<path parameter>'.

        ## Parameters:
            - `path`: path to the artifact.
            - `fp`: file-like object.
        """
        meta = self.__meta__

        self._v_storage["artifacts"].append(path)

        self._provider.write(path=f'{self._path}/{meta["v_storage_folder"]}/artifacts/{path}', fp=fp)

    def fetch_artifact(self, path: str, fp):
        """
        Fetches an artifact from the node.

        ## Parameters:
            - `path`: path to the artifact.
            - `fp`: file-like object.
        """

        meta = self.__meta__

        if path not in self._v_storage["artifacts"]:
            raise FileExistsError(f"Unregistered artifact: {path}")

        self._provider.read(path=f'{self._path}/{meta["v_storage_folder"]}/artifacts/{path}', fp=fp)
