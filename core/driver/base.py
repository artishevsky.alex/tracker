class BaseDriver:
    """
    BaseDriver is a backend driver for tracker.

    It is responsible for loading and saving data
    on a supported by driver hierarchical FileSystem
    (it shouldn't be real FS) and accessing and viewing its files/folders.

    """

    def read(self, path: str, fp):
        """
        Reads file from path and writes it to fp.

        ## Parameters:
            - path (str): path to file
            - fp (file): file object to write to

        ## Returns:
            - fp (file): file object with file content

        ## Note:
            - fp could be both binary and unicode depending so driver should handle it.
        """

        raise NotImplementedError()

    def write(self, path: str, fp):
        """
        Write fp content to file on a FileSystem.

        ## Parameters:
            - path (str): path to file
            - fp (file): file object to read from

        ## Note:
            - fp could be both binary and unicode depending so driver should handle it.
        """

        raise NotImplementedError()

    def list(self, path: str):
        """
        List files and folders in a directory.

        ## Parameters:
            - path (str): path to directory

        ## Returns:
            - files (list): list of files in directory
        """

        raise NotImplementedError()
