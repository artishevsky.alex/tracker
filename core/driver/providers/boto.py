import tempfile

import boto3

from boto3_type_annotations.s3 import Client
from boto3_type_annotations.s3.paginator import Paginator

from core.driver import BaseDriver
from core.driver.encoders import encode_binary_to_utf8, encode_utf8_to_binary


class BotoS3Driver(BaseDriver):
    """
    FileSystem operation provider based on boto3.
    """

    def __init__(self):
        self._client: Client = boto3.client("s3")

    @staticmethod
    def __parse_url(url: str):
        if url.startswith("s3://"):
            folds = url[5:].split("/")
            bucket, path = folds[0], "/".join(folds[1:])
            return bucket, path
        raise ValueError("Invalid provider")

    def read(self, path: str, fp):
        bucket, path = self.__parse_url(path)

        with tempfile.TemporaryFile() as bin_fp:
            target_fp = fp if "b" in fp.mode else bin_fp

            self._client.download_fileobj(Fileobj=target_fp, Bucket=bucket, Key=path)
            target_fp.seek(0)

            if "b" not in fp.mode:
                fp = encode_binary_to_utf8(target_fp, fp)

        fp.seek(0)

        return fp

    def write(self, path: str, fp):
        bucket, path = self.__parse_url(path)

        fp.seek(0)

        bin_fp = fp if "b" in fp.mode else encode_utf8_to_binary(fp)

        self._client.put_object(Body=bin_fp, Bucket=bucket, Key=path)

        return

    def list(self, path):
        bucket, path = self.__parse_url(path)

        files = []
        paginator: Paginator = self._client.get_paginator("list_objects_v2")
        pages = paginator.paginate(Bucket=bucket, Prefix=path, PaginationConfig={"PageSize": 1000})

        for page in pages:
            if files is None:
                files = page.get("Contents")
            else:
                files.extend(page.get("Contents"))

        path_len = len(path)

        if len(files) == 0:
            raise FileNotFoundError(f"Attempted to list non-existing s3 key prefix {path}")

        raw_files = [(file["Key"][path_len:].lstrip("/")).split("/") for file in files]
        filtered_files = [f"{file[0]}/" if len(file) > 0 else file[0] for file in raw_files]
        return filtered_files
