def encode_utf8_to_binary(fp):
    """Encode a unicode file object to binary."""

    content = fp.read()
    fp.seek(0)
    return bytes(content, encoding="utf-8")


def encode_binary_to_utf8(bin_src, unicode_dst):
    """Encode a binary file object to unicode."""

    binary_content = bin_src.read()

    unicode_content = binary_content.decode("utf-8")
    unicode_dst.flush()
    unicode_dst.write(unicode_content)

    return unicode_dst
